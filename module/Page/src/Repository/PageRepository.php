<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 28.11.2018
 * Time: 21:19
 */
namespace Page\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class PageRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function pageSave($data)
    {
        $table = new TableGateway('page_core',$this->adapter);
        return $table->insert([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'menu_id' => $data['menu_id'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => $data['status']
        ]);
    }

    public function pageList()
    {
        $dataList = [];
        $table = new TableGateway('page_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'user_id' => $item['user_id'],
                'menu_id' => $item['menu_id'],
                'create_date' => $item['create_date'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function pageFindByOne($id)
    {
        $dataList = [];
        $table = new TableGateway('page_core',$this->adapter);
        $rows = $table->select(sprintf('id=%d',$id));

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'content' => $item['content'],
                'user_id' => $item['user_id'],
                'menu_id' => $item['menu_id'],
                'create_date' => $item['create_date'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function pageRemove($id)
    {
        $table = new TableGateway('page_core',$this->adapter);
        return $table->delete([
            'id' => $id
        ]);
    }

    public function pageEdit($data)
    {
        $table = new TableGateway('page_core',$this->adapter);
        return $table->update($data,[
            'id' => $data['id']
        ]);
    }
}