<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Blog\Controller;

use Blog\Form\BlogCreateForm;
use Blog\Repository\BlogRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PanelController extends AbstractActionController
{
    private $blogRepository;

    public function __construct()
    {
        $this->blogRepository = new BlogRepository();
    }

    public function indexAction()
    {
        $title = 'Blog Listesi';

        $view = new ViewModel([
            'title' => $title
        ]);
        $view->setTemplate('page/panel/blog-index');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title
        ]);
        return $view;
    }

    public function createAction()
    {
        $form = new BlogCreateForm();
        $title = 'Blog Oluştur';
        $view = new ViewModel([
            'title' => $title,
            'form' => $form
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $result = $this->blogRepository->blogSave($formData);
            if($result)
            {
                return $this->redirect()->toRoute('panel/blog');
            }
        }

        $view->setTemplate('page/panel/blog-create');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title
        ]);
        return $view;
    }

    public function xhrAction()
    {
        $dataList = [];
        $view = new ViewModel();

        $i = 1;
        foreach ($this->blogRepository->blogList() as $item)
        {
            $dataList['aaData'][] = [
                $i++,
                $item['title'],
                'KATEGORİ',
                $item['create_date'],
                $item['status'] == 'Y' ? '<button type="button" class="btn btn-success btn-xs btn-block">Aktif</button>' : '<button type="button" class="btn danger btn-xs btn-block">Pasif</button>',
                $item['id']
            ];
        }

        $view->setVariables([
            'data' => json_encode($dataList)
        ]);

        $view->setTemplate('page/panel/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function removeAction()
    {
        $id = $this->params()->fromRoute('id');

        $result = $this->blogRepository->blogRemove($id);
        if($result)
        {
            $_SESSION['message'] = [
                'code' => 200,
                'title' => 'Başarılı',
                'text' => 'İşleminiz başarılı bir şekilde gerçekleştirilmiştir.'
            ];
            return $this->redirect()->toRoute('panel/blog');
        }else{
            $_SESSION['message'] = [
                'code' => 403,
                'title' => 'Uyarı',
                'text' => 'İşleminiz sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
            ];
            return $this->redirect()->toRoute('panel/blog');
        }
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');

        $form = new BlogCreateForm();
        $form->setData($this->blogRepository->blogFindByOne($id));
        $title = 'Blog Oluştur';
        $view = new ViewModel([
            'title' => $title,
            'form' => $form
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formData = array_merge($formData,['id' => $id]);

            $result = $this->blogRepository->blogEdit($formData);
            if($result)
            {
                return $this->redirect()->toRoute('panel/blog');
            }
        }

        $view->setTemplate('page/panel/blog-create');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title
        ]);
        return $view;
    }
}
