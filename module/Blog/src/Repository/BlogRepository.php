<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 28.11.2018
 * Time: 21:19
 */
namespace Blog\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class BlogRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function blogSave($data)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->insert([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => $data['status']
        ]);
    }

    public function blogList()
    {
        $dataList = [];
        $table = new TableGateway('blog_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'user_id' => $item['user_id'],
                'create_date' => $item['create_date'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function blogFindByOne($id)
    {
        $dataList = [];
        $table = new TableGateway('blog_core',$this->adapter);
        $rows = $table->select(sprintf('id=%d',$id));

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'content' => $item['content'],
                'user_id' => $item['user_id'],
                'create_date' => $item['create_date'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function blogRemove($id)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->delete([
            'id' => $id
        ]);
    }

    public function blogEdit(array $data)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->update($data,[
            'id' => $data['id']
        ]);
    }
}