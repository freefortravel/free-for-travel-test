<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 28.11.2018
 * Time: 21:10
 */
namespace Blog\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class BlogCreateForm extends Form
{
    public function __construct(string $name = null, array $config = [])
    {
        parent::__construct($name,$config);

        $this
            ->add([
                'type' => Text::class,
                'name' => 'title',
                'options' => [
                    'label' => 'Başlık'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ])
            ->add([
                'type' => Textarea::class,
                'name' => 'content',
                'options' => [
                    'label' => 'İçerik'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'id' => 'textEditor'
                ]
            ])
            ->add([
                'type' => Select::class,
                'name' => 'status',
                'options' => [
                    'label' => 'Durum',
                    'value_options' => [
                        'Y' => 'Aktif',
                        'N' => 'Pasif'
                    ]
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]);
    }
}