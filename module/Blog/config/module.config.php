<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Blog;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'blog' => [
                        'type' => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'blog[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ]
            ],
            'panel' => [
                'child_routes' => [
                    'blog' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'blog[/]',
                            'defaults' => [
                                'controller' => Controller\PanelController::class,
                                'action'     => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'create' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'create[/]',
                                    'defaults' => [
                                        'controller' => Controller\PanelController::class,
                                        'action'     => 'create',
                                    ],
                                ],
                            ],
                            'remove' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'remove/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\PanelController::class,
                                        'action'     => 'remove',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'edit/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\PanelController::class,
                                        'action'     => 'edit',
                                    ],
                                ],
                            ],
                            'xhr' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'xhr[/]',
                                    'defaults' => [
                                        'controller' => Controller\PanelController::class,
                                        'action'     => 'xhr',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\SiteController::class => InvokableFactory::class,
            Controller\PanelController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
