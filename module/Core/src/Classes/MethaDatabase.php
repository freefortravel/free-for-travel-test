<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU <mehmethakkioglu@yandex.com>
 * Date: 01/03/2017
 * Time: 17:44
 * Description:
 */

namespace Core\Classes;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;

class MethaDatabase
{
    public function MethaDatabaseConnection()
    {
        $adapter = new Adapter([
            'driver'   => 'Mysqli',
            'database' => 'freetravel',
            'username' => 'root',
            'password' => 'pct1616',
            'charset' => 'utf8'
        ]);

        return $adapter;
    }

    function methaSlug($text) {
        $text = trim($text);
        $search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ','I');
        $replace = array('c','c','g','g','i','i','o','o','s','s','u','u','-','i');
        $new_text = str_replace($search,$replace,$text);
        return strtolower($new_text);
    }
}