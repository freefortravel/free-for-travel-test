<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => 'Hoşgeldiniz...'
        ]);
        return $view;
    }

    public function introAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/intro');
        $this->layout()->setTemplate('layout/intro')->setVariables([
            'title' => 'Hoşgeldiniz...'
        ]);
        return $view;
    }
}
