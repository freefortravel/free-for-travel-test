<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 20.11.2018
 * Time: 23:02
 */
namespace Core\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class BlogMediaRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function blogSave($data)
    {
        $table = new TableGateway('blog_media',$this->adapter);
        return $table->insert([
            'blog_id' => $data['blog_id'],
            'media_type' => $data['media_type'],
            'url' => $data['url']
        ]);
    }

    public function blogRemove($id)
    {
        $table = new TableGateway('blog_media',$this->adapter);
        return $table->delete([
            'id' => $id
        ]);
    }

    public function blogList($query = null)
    {
        $dataList = [];
        $table = new TableGateway('blog_media',$this->adapter);
        $rows = $table->select($query != null ? $query : null);

        foreach ($rows as $item)
        {
            $dataList[] = [
                'blog_id' => $item['blog_id'],
                'media_type' => $item['media_type'],
                'url' => $item['url']
            ];
        }

        return $dataList;
    }

    public function blogUpdate($data)
    {
        $table = new TableGateway('blog_media',$this->adapter);
        return $table->update([
            'media_type' => $data['media_type'],
            'url' => $data['url']
        ],[
            'id' => $data['id']
        ]);
    }
}