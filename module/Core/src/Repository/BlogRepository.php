<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 20.11.2018
 * Time: 23:02
 */
namespace Core\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class BlogRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function blogSave($data)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->insert([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => $data['status']
        ]);
    }

    public function blogRemove($id)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->delete([
            'id' => $id
        ]);
    }

    public function blogList($query = null)
    {
        $dataList = [];
        $table = new TableGateway('blog_core',$this->adapter);
        $rows = $table->select($query != null ? $query : null);

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'content' => $item['content'],
                'user_id' => $item['user_id'],
                'create_date' => $item['create_date'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function blogUpdate($data)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->update([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'content' => $data['content'],
            'status' => $data['status']
        ],[
            'id' => $data['id']
        ]);
    }
}