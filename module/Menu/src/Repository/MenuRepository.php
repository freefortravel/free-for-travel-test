<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 28.11.2018
 * Time: 21:19
 */
namespace Menu\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class MenuRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function menuSave($data)
    {
        $table = new TableGateway('menu_core',$this->adapter);
        return $table->insert([
            'title' => $data['title'],
            'slug' => $data['slug'],
            'menu_type' => $data['menu_type'],
            'parent_id' => $data['parent_id'],
            'status' => $data['status']
        ]);
    }

    public function menuList()
    {
        $dataList = [];
        $table = new TableGateway('menu_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'menu_type' => $item['menu_type'],
                'parent_id' => $item['parent_id'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function menuFindByOne($id)
    {
        $dataList = [];
        $table = new TableGateway('menu_core',$this->adapter);
        $rows = $table->select(sprintf('id=%d',$id));

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'menu_type' => $item['menu_type'],
                'parent_id' => $item['parent_id'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function menuRemove($id)
    {
        $table = new TableGateway('menu_core',$this->adapter);
        return $table->delete([
            'id' => $id
        ]);
    }

    public function menuEdit($data)
    {
        $table = new TableGateway('menu_core',$this->adapter);
        return $table->update($data,[
            'id' => $data['id']
        ]);
    }
}