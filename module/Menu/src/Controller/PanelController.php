<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Menu\Controller;

use Core\Classes\MethaDatabase;
use Menu\Form\MenuCreateForm;
use Menu\Repository\MenuRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PanelController extends AbstractActionController
{
    private $blogRepository;
    private $methaDatabase;

    public function __construct()
    {
        $this->blogRepository = new MenuRepository();
        $this->methaDatabase = new MethaDatabase();
    }

    public function indexAction()
    {
        $title = 'Menü Listesi';

        $view = new ViewModel([
            'title' => $title
        ]);
        $view->setTemplate('page/panel/menu-index');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title
        ]);
        return $view;
    }

    public function createAction()
    {
        $form = new MenuCreateForm();
        $title = 'Menü Oluştur';
        $view = new ViewModel([
            'title' => $title,
            'form' => $form
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if($formData['menu_type'] == 'core')
            {
                $formData = array_merge($formData,['parent_id' => 0]);
            }

            $formData = array_merge($formData,['slug' => $this->methaDatabase->methaSlug($formData['title'])]);

            $result = $this->blogRepository->menuSave($formData);
            if($result)
            {
                return $this->redirect()->toRoute('panel/menu');
            }
        }

        $view->setTemplate('page/panel/menu-create');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title
        ]);
        return $view;
    }

    public function xhrAction()
    {
        $dataList = [];
        $view = new ViewModel();

        $i = 1;
        foreach ($this->blogRepository->menuList() as $item)
        {
            $dataList['aaData'][] = [
                $i++,
                $item['title'],
                'KATEGORİ',
                $item['create_date'],
                $item['status'] == 'Y' ? '<button type="button" class="btn btn-success btn-xs btn-block">Aktif</button>' : '<button type="button" class="btn danger btn-xs btn-block">Pasif</button>',
                $item['id']
            ];
        }

        $view->setVariables([
            'data' => json_encode($dataList)
        ]);

        $view->setTemplate('page/panel/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function removeAction()
    {
        $id = $this->params()->fromRoute('id');

        $result = $this->blogRepository->menuRemove($id);
        if($result)
        {
            $_SESSION['message'] = [
                'code' => 200,
                'title' => 'Başarılı',
                'text' => 'İşleminiz başarılı bir şekilde gerçekleştirilmiştir.'
            ];
            return $this->redirect()->toRoute('panel/menu');
        }else{
            $_SESSION['message'] = [
                'code' => 403,
                'title' => 'Uyarı',
                'text' => 'İşleminiz sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
            ];
            return $this->redirect()->toRoute('panel/menu');
        }
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');

        $form = new MenuCreateForm();
        $form->setData($this->blogRepository->menuFindByOne($id));
        $title = 'Menü Oluştur';
        $view = new ViewModel([
            'title' => $title,
            'form' => $form
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formData = array_merge($formData,['id' => $id,'slug' => $this->methaDatabase->methaSlug($formData['title'])]);

            $result = $this->blogRepository->menuEdit($formData);
            if($result)
            {
                return $this->redirect()->toRoute('panel/menu');
            }
        }

        $view->setTemplate('page/panel/menu-create');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title
        ]);
        return $view;
    }
}
